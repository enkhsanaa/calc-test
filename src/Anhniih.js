import './App.css';
import { useState } from "react";
import { Tootsoolol } from './Tootsoolol';
// import { Form, Input, Select } from "./Components";

function useInput({ type, name /*...*/ }) {
  const [value, setValue] = useState("");

  const input = <div class="m-1">
    <label class="block text-gray-700 text-sm font-bold mb-2" for={name}>
      {name}
    </label>
    <input id={name} class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" value={value} onChange={e => setValue(e.target.value)} type={type} />
    {/* <p class="text-red-500 text-xs italic">x1 utga.</p> */}
  </div>
  return [value, input];
}

function App() {
  const [uiM, showM] = useInput({ type: "number", name: 'M' });
  const [uic, showc] = useInput({ type: "number", name: 'c' });
  const [uiW, showW] = useInput({ type: "number", name: 'W' });
  const [uiX1, showGamma] = useInput({ type: "number", name: 'x1' });
  const [uiAnswer, setAnswer] = useState(0)

  const calculateThisShit = (evt) => {
    evt.preventDefault();

    const M = parseFloat(uiM)
    const c = parseFloat(uic)

    const W = parseFloat(uiW)
    const Q = parseFloat(uiX1)
    
    const answer = Tootsoolol({M, W, Q, c})

    setAnswer(answer.toFixed(3))
  }
  return <>
    <div className="p-12 flex w-100 h-100 flex-col items-start">
      <div className="flex">
        {showM}
        {showc}
        {showW}
        {showGamma}
      </div>
      <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold m-2 py-2 px-4 rounded focus:outline-none focus:shadow-outline" onClick={calculateThisShit} value="Submit">Calculate</button>
      <div class="max-w-sm mx-auto bg-white shadow-lg rounded-lg overflow-hidden">
        <div class="sm:flex flex-col sm:items-center px-6 py-4">
          <img class="block mx-auto sm:mx-0 sm:flex-shrink-0 h-16 sm:h-24 mb-10" src="https://latex.codecogs.com/svg.image?\frac{M}{W&space;*&space;\gamma&space;}" title="\frac{M}{W * \gamma }" />
          <div class="mt-4 sm:mt-0 sm:ml-4 text-center">
            <p class="text-xl leading-tight">
              Хариулт
              {/* {showM} */}
            </p>
            <div class="mt-4">
              <button class="text-purple-500 hover:text-white hover:bg-purple-500 border border-purple-500 text-xs font-semibold rounded-full px-4 py-1 text-3xl">
                {uiAnswer}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </>;
}

export default App;
