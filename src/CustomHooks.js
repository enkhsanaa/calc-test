import { useState } from "react";

export function useInput({ name, type, defaultValue }) {
    const [value, setValue] = useState(defaultValue);

    const input = <div class="m-1">
        <label class="block text-gray-700 text-sm font-bold mb-2" for={name}>
            {name}
        </label>
        <input id={name} class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
            value={value} onChange={e => {
                setValue(e.target.value);

            }} type={type} />
        {/* <p class="text-red-500 text-xs italic">x1 utga.</p> */}
    </div>
    return [value, input];
}

export function useSelect({ name, options, defaultValue }) {
    const [value, setValue] = useState(options[defaultValue ?? 0].value);

    const input = <div className="mr-5">
        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2 break-normal" for={name}>
            {name}
        </label>
        <div class="relative">
            <select class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                id={name} onChange={e => {
                    setValue(e.target.value);
                }} >
                {options.map((option, i) => {
                    if (i === defaultValue) {
                        return (<option key={name + "" + i} value={option.value} selected> {option.name}</option>)
                    }
                    return (<option key={name + "" + i} value={option.value}> {option.name}</option>)
                })}
            </select>
            <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg>
            </div>
        </div>
    </div >
    return [value, input];
}

export function useRadio({ name, options, defaultValue }) {
    const [dirty, setDirty] = useState(false);
    const [value, setValue] = useState(options[defaultValue ?? 0].value);

    const input = <div class="block shadow-lg m-3 p-5 rounded">
        <span class="text-gray-700">{name}</span>
        <div class="mt-2">
            {options.map((option, i) => {
                if (dirty === false && i === defaultValue) {
                    return (<div>
                        <label class="inline-flex items-center">
                            <input type="radio" class="form-radio" name={name} value={option.value} checked onChange={e => { setValue(e.target.value); setDirty(true); }} />
                            <span class="ml-2">{option.name}</span>
                        </label>
                    </div>)
                }
                return <div>
                    <label class="inline-flex items-center">
                        <input type="radio" class="form-radio" name={name} value={option.value} onChange={e => { setValue(e.target.value); setDirty(true); }} />
                        <span class="ml-2">{option.name}</span>
                    </label>
                </div>
            })}
        </div>
    </div>
    return [value, input];
}