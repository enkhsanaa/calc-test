
export const Tootsoolol = ({ alpha, ds, Rs, n1, n2, Rbt, kucn }) => {
    const result = (alpha / 4) * ((ds * Rs) / (n1 * n2 * Rbt)) * kucn
    return result.toFixed(4)
}