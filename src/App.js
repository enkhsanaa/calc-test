import './App.css';
import { useEffect, useState } from "react";
import { Tootsoolol } from './Tootsoolol';
import { useInput, useRadio, useSelect } from './CustomHooks';


function App() {
  const armaturuud = [
    { name: 'A240', value: '215' },
    { name: 'A300', value: '270' },
    { name: 'A400', value: '355' },
    { name: 'A500', value: '435' },
    { name: 'B500', value: '435' },
    { name: 'SR235', value: '210' },
    { name: 'SR295', value: '265' },
    { name: 'SD295', value: '265' },
    { name: 'SD345', value: '310' },
    { name: 'SD390', value: '350' },
    { name: 'SD490', value: '430' }
  ]
  const betoniiAngiuud = [
    { name: 'B10', value: '0.56' },
    { name: 'B15', value: '0.75' },
    { name: 'B20', value: '0.9' },
    { name: 'B25', value: '1.05' },
    { name: 'B30', value: '1.15' },
    { name: 'B35', value: '1.3' },
    { name: 'B40', value: '1.4' },
    { name: 'B45', value: '1.5' },
    { name: 'B50', value: '1.6' },
    { name: 'B55', value: '1.7' },
    { name: 'B60', value: '1.8' }
  ]
  const diametruud = [
    { name: '6', value: '6' },
    { name: '8', value: '8' },
    { name: '10', value: '10' },
    { name: '12', value: '12' },
    { name: '14', value: '14' },
    { name: '16', value: '16' },
    { name: '18', value: '18' },
    { name: '20', value: '20' },
    { name: '22', value: '22' },
    { name: '25', value: '25' },
    { name: '28', value: '28' },
    { name: '32', value: '32' },
    { name: '36', value: '36' },
    { name: '40', value: '40' },
  ]
  const torluud = [
    { name: 'Анкерлалт', value: 0 },
    { name: 'Зөрүүлэг', value: 1 }
  ]
  const ajillaganuud = [
    { name: 'Суналтанд', value: 0 },
    { name: 'Шахалтанд', value: 1 }
  ]
  const bolovsruulaltuud = [
    { name: 'Гөлгөр гадаргуутай боловсруулалт', value: 1.5 },
    { name: 'Хүйтэнээр хэв гажуулсан', value: 2 },
    { name: 'Халуунаар цувисан', value: 2.5 },
  ]
  const matrix = [
    [1, 1.2],
    [0.75, 0.9]
  ]
  const [uiArmatur, uiArmaturHaruul] = useSelect({ name: 'Арматур', options: armaturuud, defaultValue: 2 })
  const [uiBeton, uiBetonHaruul] = useSelect({ name: 'Бетоны анги', options: betoniiAngiuud, defaultValue: 3 })
  const [uiDiametr, uiDiametrHaruul] = useSelect({ name: 'Диаметр', options: diametruud, defaultValue: 5 })
  const [uiAscal, uiAscalHaruul] = useInput({ type: 'number', name: 'Ascal/Asref', defaultValue: 1 })
  const [uiTorol, uiTorolHaruul] = useRadio({ name: 'Төрөл', options: torluud, defaultValue: 0 })
  const [uiAjillagaa, uiAjillagaaHaruul] = useRadio({ name: 'Бүтээцийн ажиллагаа', options: ajillaganuud, defaultValue: 0 })
  const [uiBolovsruulalt, uiBolovsruulaltHaruul] = useRadio({ name: 'Арматурын боловсруулалт', options: bolovsruulaltuud, defaultValue: 2 })

  const [uiAnswer, setUiAnswer] = useState("")
  const [uiFormula, setUiFormula] = useState("")


  const calculateThisShit = (evt) => {
    evt?.preventDefault();
    console.log('called')

    const alpha = matrix[uiAjillagaa][uiTorol]
    const ds = parseFloat(uiDiametr)
    const Rs = parseFloat(uiArmatur)
    const n1 = parseFloat(uiBolovsruulalt)
    const n2 = ds > 32 ? 0.9 : 1.0
    const Rbt = parseFloat(uiBeton)
    const kucn = parseFloat(uiAscal)

    const answer = Tootsoolol({ alpha, ds, Rs, n1, n2, Rbt, kucn })
    setUiFormula(`https://latex.codecogs.com/svg.image?L_{an}=\\frac{${alpha}}{4}*\\frac{${ds}*${Rs}}{${n1}*${n2}*${Rbt}}*${kucn}=${answer}`)
    setUiAnswer(answer)
  }
  useEffect(() => {
    calculateThisShit()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  return <>
    <div className="p-12 flex w-full h-full justify-around">
      <div>
        <div className="rounded overflow-hidden shadow-lg p-10 m-2">
          <div className="font-bold text-xl mb-2">Оролт</div>
          <div className="flex justify-between">
            {uiArmaturHaruul}
            {uiBetonHaruul}
            {uiDiametrHaruul}
            {uiAscalHaruul}
          </div>
          <div className="flex">
            {uiTorolHaruul}
            {uiAjillagaaHaruul}
            {uiBolovsruulaltHaruul}
          </div>
          <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold m-2 py-2 px-4 rounded focus:outline-none focus:shadow-outline" onClick={calculateThisShit} value="Submit">Calculate</button>
        </div>
        <div className="rounded overflow-hidden shadow-lg p-10 m-2">
          <div className="font-bold text-xl mb-2">Formula</div>
          <div className="sm:flex flex-col sm:items-center px-6 py-4">
            <img style={{ maxWidth: '600px' }} className="block mx-auto sm:mx-0 sm:flex-shrink-0 h-16 sm:h-24 mb-10"
              src={uiFormula} title="Equation 2" alt="Equation Filled" />

            <div className="mt-4 sm:mt-0 sm:ml-4 text-center">
              <p className="text-xl leading-tight">
                Хариулт
              </p>
              <div className="mt-4">
                <button className="text-purple-500 hover:text-white hover:bg-purple-500 border border-purple-500 text-xs font-semibold rounded-full px-4 py-1 text-3xl">
                  {uiAnswer}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div className=" overflow-hidden shadow-lg p-10 m-2 flex-1">
          <div className="font-bold text-xl mb-2">Оролт</div>
          <img src="./cheatsheet.JPG" alt="cheatsheet" />
        </div>
        <div className="rounded overflow-hidden shadow-lg p-10 m-2">
          <div className="font-bold text-xl mb-2">Formula</div>
          <div className="sm:flex flex-col sm:items-center px-6 py-4">
            <img style={{ maxWidth: '400px' }} className="block mx-auto sm:mx-0 sm:flex-shrink-0 h-16 sm:h-24 mb-10"
              src={`https://latex.codecogs.com/svg.image?L_{an}=\\alpha*\\frac{A_{s}R_{s}}{u_{s}\\eta_{1}\\eta_{2}R_{bt}}*\\frac{A_{s,cal}}{A_{s,ef}}`} title="Equation" alt="Equation" />
            <img style={{ maxWidth: '400px' }} className="block mx-auto sm:mx-0 sm:flex-shrink-0 h-16 sm:h-24 mb-10"
              src={`https://latex.codecogs.com/svg.image?L_{an}=\\frac{\\alpha}{4}*\\frac{d_{s}R_{s}}{\\eta_{1}\\eta_{2}R_{bt}}*k_{ucn}`} title="Equation" alt="Equation" />

          </div>
        </div>
      </div>

    </div>
  </>;
}

export default App;
